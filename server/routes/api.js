const express = require('express');
const router = express.Router();
const getRooms = () => Promise.resolve([...require('../io').rooms])

router.get('/users', (req, res, next) => {
  getRooms()
    .then(rooms => {
      const dto = rooms
        .map(room => room.occupants.map(x => Object.assign({}, x, { room: room.name })))
        .reduce((a, c) => a.concat(c), [])
        .filter(occupant => occupant.online)
        .map(occupant => ({
          id: occupant.user.id,
          username: occupant.user.name,
          room: occupant.room
        }))
      res.json(dto)
    })
    .catch(next)
})

router.get('/users/:id', (req, res, next) => {
  getRooms()
    .then(rooms => {
      const id = req.params.id
      const dto = rooms
        .map(room => room.occupants)
        .reduce((a, c) => a.concat(c), [])
        .map(occupant => occupant.user)
        .find(user => user.id === id)
      res.json(dto)
    })
    .catch(next)
})

router.get('/rooms', (req, res, next) => {
  getRooms()
    .then(rooms => {
      const dto = rooms.map(room => room.name)
      res.json(dto)
    })
    .catch(next)
})

router.get('/rooms/:name', (req, res, next) => {
  getRooms()
    .then(rooms => {
      const name = req.params.name
      const room = rooms.find(x => x.name === name)
      const dto = room && {
        name: room.name,
        occupants: room.occupants.map(x => ({
          username: x.user.name,
          userId: x.user.id,
          messags: x.messages
        }))
      }

      res.json(dto)
    })
    .catch(next)
})

module.exports = router
