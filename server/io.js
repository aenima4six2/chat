const socketIo = require('socket.io')
const debug = require('debug')('server:server')
const Room = require('./models/Room')
const rooms = new Set()

module.exports.rooms = rooms
module.exports.start = (server) => {
    // Connect  
    const io = socketIo(server)
    debug('Started Socket.io server')

    io.on('connection', socket => {
        // Join
        socket.on('join', ({user, roomName}) => {
            debug(`User ${user.name} joined.`)

            // Add channel and user
            const room = getOrCreateRoom(roomName)
            room.addUser(user, socket)
        })
    })
}

const getOrCreateRoom = name => {
    let room = [...rooms].find(x => x.name === name)
    if (!room) {
        debug(`Room ${name} not found! Creating.`)
        room = new Room(name, debug)
        rooms.add(room)
    }

    return room
}