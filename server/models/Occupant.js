class Occupant {
    constructor(user, socket) {
        this.user = user
        this.socket = socket
        this.messages = []
        this.online = true
    }
}

module.exports = Occupant