const Occupant = require('./Occupant')

class Room {
    constructor(name, logger) {
        this.name = name
        this._occupants = new Set()
        this.logger = logger
    }

    get occupants() {
        return [...this._occupants]
    }

    addUser(user, socket) {
        // Add user and socket
        const existing = this.occupants.find(x => x.user.id === user.id)
        const occupant = existing || new Occupant(user, socket)
        if (!existing) this._occupants.add(occupant)
        else occupant.socket = socket
        this._subscribe(occupant)
    }

    _subscribe(occupant) {
        // Subscribe to room
        const socket = occupant.socket
        socket.join(this.name)

        // Send all active users
        const otherUsers = this.occupants
            .filter(x => x.user.id !== occupant.user.id && x.online)
            .map(x => x.user)
        socket.emit('active users', otherUsers)

        // Send chat history
        const history = this.occupants
            .map(x => x.messages || [])
            .reduce((a, c) => [...a, ...c], [])
            .sort((a, b) => a.date - b.date)
        socket.emit('message history', history)

        // Notify other users that a new user joined
        socket.broadcast.to(this.name).emit('user joined', occupant.user)

        // Subscribe to messages
        socket.on('send message', message => {
            this.logger(`message recieved from ${occupant.user.name}`, message)
            this._sendMessage(occupant, message)
        })

        //Disconnect
        socket.on('disconnect', reason => {
            this.logger(`user ${occupant.user.name} disconnected`);
            occupant.online = false
            occupant.socket = undefined
            socket.broadcast.to(this.name).emit('user left', occupant.user)
        });
    }

    _sendMessage(occupant, message) {
        const socket = occupant.socket
        occupant.messages.push(message)
        socket.broadcast.to(this.name).emit('recieve message', message)
        this.logger(`message sent from ${occupant.user.name} to room`, message)
    }
}

module.exports = Room