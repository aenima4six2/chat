import React from 'react'

export default class MessageInput extends React.Component {
    sendMessage = (e) => {
        e.preventDefault()
        const text = this.props.messageText
        if (text && text.trim() !== '') {
            this.props.sendMessage(text, this.props.currentUser)
            this.props.resetInput()
        }
    }

    onInputKeyDown = (e) => {
        if (e.keyCode === 13) {
            this.sendMessage(e)
        }
    }

    onInputChanged = (e) => {
        e.preventDefault()
        const text = e.target.value
        this.props.inputUpdated(text)
    }

    render() {
        return (
            <div className="chat-window-controls">
                <div className="chat-input">
                    <input
                        className="text chat"
                        type="text"
                        value={this.props.messageText}
                        placeholder="Enter message..."
                        onChange={this.onInputChanged}
                        onKeyDown={this.onInputKeyDown}
                        />
                    <button className="btn" onClick={this.sendMessage}>
                        <i className="fa fa-envelope" aria-hidden="true"></i>
                    </button>
                </div>
            </div>
        )
    }
}

MessageInput.propTypes = {
    messageText: React.PropTypes.string,
    sendMessage: React.PropTypes.func,
    inputUpdated: React.PropTypes.func,
    currentUser: React.PropTypes.object,
    resetInput: React.PropTypes.func
}