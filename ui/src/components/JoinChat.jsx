import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { withRouter } from 'react-router'
import { apiBaseAddress } from '../utils/constants'
import * as chatActions from '../actions/chatActions'
import fetch from 'fetch-polyfill'

class JoinChat extends React.Component {
    componentDidMount() {
        this.input.focus()
    }

    join = (e) => {
        e.preventDefault()
        const channel = this.props.channelName
        this.props.router.push(`/chat/${channel}`)
    }

    onInputKeyDown = (e) => {
        if (e.keyCode === 13) {
            this.join(e)
        }
    }

    onInputChanged = (e) => {
        e.preventDefault()
        const channelName = e.target.value
        if(channelName && channelName.trim()){
            this.props.editChannelName(channelName.toLowerCase())
        }
    }

    render() {
        return (
            <div className="chat-join">
                <h1>Join a chat room!</h1>
                <div className="chat-input">
                    <label for="#rooms">Existing Rooms</label>
                    <ul id="rooms">
                        <li>Chat rooms here</li>
                    </ul>   
                </div>
                <div className="chat-input">
                    <input
                        ref={node => this.input = node}
                        className="text join"
                        type="text"
                        placeholder="Enter a room name..."
                        value={this.props.channelName}
                        onChange={this.onInputChanged}
                        onKeyDown={this.onInputKeyDown}
                        />
                    <a href='#' className="btn" onClick={this.join}>
                        Join
                    </a>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        channelName: state.chat.channelName
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        editChannelName: chatActions.editChannelName
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(JoinChat));
