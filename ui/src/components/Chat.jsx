import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Users from './Users'
import Messages from './Messages'
import MessageInput from './MessageInput'

import * as inputActions from '../actions/inputActions'
import * as messageActions from '../actions/messageActions'
import * as chatActions from '../actions/chatActions'

import '../style/app.css'
import '../../node_modules/font-awesome/css/font-awesome.css'

class Chat extends Component {
    componentDidMount() {
        const channel = this.props.params.channel
        this.props.joinChannel(channel)
    }

    leaveChannel = () => {
        const channel = this.props.params.channel
        this.props.leaveChannel(channel)
        this.props.router.push(`/join`)
    }

    render() {
        return (
            <div className="chat-window">
                <Users users={this.props.users} />
                <div className="chat-window-user-controls">
                    <button className='btn' onClick={this.leaveChannel}>Leave</button>
                </div>
                <Messages
                    messages={this.props.messages}
                    users={this.props.users}
                    />
                <MessageInput
                    messageText={this.props.messageinput.messageText}
                    currentUser={this.props.users.find(x => x.isCurrent)}
                    sendMessage={this.props.sendMessage}
                    inputUpdated={this.props.inputUpdated}
                    resetInput={this.props.resetInput}
                    />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        messages: state.messages.messages,
        users: state.users.users,
        messageinput: state.messageInput
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        sendMessage: messageActions.createMessage,
        inputUpdated: inputActions.inputUpdated,
        resetInput: inputActions.resetInput,
        joinChannel: chatActions.joinChannel,
        leaveChannel: chatActions.leaveChannel
    }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
