import React from 'react'
import { ButtonToolbar, Button } from 'react-bootstrap'
import '../style/login.css'

export default class Login extends React.Component {
    render() {
        const auth = this.props.auth
        return (
            <div className="root">
                <h2>Welcome to Simple Chat!</h2>
                <h4>Click the login button below to get started</h4>
                <ButtonToolbar className="toolbar">
                    <Button bsStyle="primary" onClick={auth.login}>Login</Button>
                </ButtonToolbar>
            </div>
        )
    }
}