import React from 'react'

export default class Messages extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.renderMessages = this.renderMessages.bind(this)
        this.getHeader = this.getHeader.bind(this)
        this.getUser = this.getUser.bind(this)
    }

    componentWillUpdate() {
        if (this.ul) {
            this.shouldScrollBottom = this.ul.scrollTop + this.ul.offsetHeight === this.ul.scrollHeight;
        }
    }

    componentDidUpdate() {
        if (this.ul && this.shouldScrollBottom) {
            this.ul.scrollTop = this.ul.scrollHeight
        }
    }

    getUser(id) {
        return this.props.users.find(x => x.id === id) || {}
    }

    getHeader(msg) {
        const options = { year: "numeric", month: "short", day: "numeric", hour: "2-digit", minute: "2-digit" };
        const dateTime = new Date(msg.date).toLocaleTimeString('en-us', options)
        return ` ${msg.username} - ${dateTime}`
    }

    renderMessages() {
        return this.props.messages.sort((a, b) => a.date - b.date).map((msg, idx) =>
            <li key={idx}>
                <div className={this.getUser(msg.userId).isCurrent ? 'message-sent' : 'message-received'}>
                    <div className="user">
                        <i className="fa fa-user" aria-hidden="true"></i>
                        {this.getHeader(msg)}
                    </div>
                    {msg.text}
                </div>
            </li>
        )
    }

    render() {
        return (
            <div className="chat-window-messages">
                <ul ref={node => this.ul = node}>{this.renderMessages()}</ul>
            </div>
        )
    }
}

Messages.propTypes = {
    messages: React.PropTypes.array.isRequired,
    users: React.PropTypes.array.isRequired
}