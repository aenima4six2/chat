import React from 'react'

export default class Users extends React.Component {
    constructor(props, context) {
        super(props, context)
        this.renderUsers = this.renderUsers.bind(this)
    }

    renderUsers() {
        return this.props.users.filter(x => x.available).map((user, idx) =>
            <li key={idx}>
                <div className="user">
                    <i className="fa fa-user" aria-hidden="true"></i>
                    {` ${user.name} ${user.isCurrent ? ' (You)' : ''}`}
                </div>
            </li>
        )
    }

    render() {
        return (
            <div className="chat-window-users">
                <span className="user">
                    <h3><i className="fa fa-users" aria-hidden="true"></i> Online Users</h3>
                </span>
                <ul>{this.renderUsers()}</ul>
            </div>
        )
    }
}

Users.propTypes = {
    users: React.PropTypes.array
}