import React, { Component } from 'react'
import { connect } from 'react-redux'

import * as chatActions from '../actions/chatActions'
import * as userActions from '../actions/userActions'

import { createCurrentUser } from '../utils/userAdapter'

import '../style/app.css'
import '../../node_modules/font-awesome/css/font-awesome.css'

class App extends Component {
  constructor(props, context) {
    super(props, context)
    this.props.route.auth.on('profile_updated', profile => {
      if (profile) {
        const user = createCurrentUser(profile)
        this.props.dispatch(userActions.createUser(user, true))
      }
    })
  }

  logout = () => {
    this.props.route.auth.logout()
    this.props.dispatch(chatActions.leaveChat())
  }

  renderStatus() {
    const user = this.props.currentUser
    return !user ? null :
      <div id="login">
        {`${user.name}: `}
        <a href="#" onClick={this.logout}>Logout</a>
      </div>
  }

  render() {
    const children = this.props.children
      ? React.cloneElement(this.props.children, {
        auth: this.props.route.auth
      })
      : null

    return (
      <div className="app">
        <header className="app-header">
          <div>
            <h1>
              <i className="fa fa-comments" aria-hidden="true"></i> Simple Chat app
            </h1>
          </div>
          {this.renderStatus()}
        </header>
        <section className="app-content">
          {children}
        </section>
        <footer className="app-footer"></footer>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    currentUser: state.users.users.find(x => x.isCurrent)
  }
}

export default connect(mapStateToProps, null)(App)