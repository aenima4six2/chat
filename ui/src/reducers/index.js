import { combineReducers, createStore, applyMiddleware } from 'redux'
import { sendMessages } from '../middleware'

import messagesReducer from './messagesReducer'
import messageInputReducer from './messageInputReducer'
import usersReducer from './usersReducer'
import chatReducer from './chatReducer'

import { createCurrentUser } from '../utils/userAdapter'

const middleware = applyMiddleware(sendMessages)
const rootReducer = combineReducers({
  messages: messagesReducer,
  messageInput: messageInputReducer,
  users: usersReducer,
  chat: chatReducer
})

export default (auth) => {
  const initialState = {}
  const profile = auth.getProfile()
  if (profile) {
    initialState.users = {
      users: [createCurrentUser(profile)]
    }
  }

  return createStore(rootReducer, initialState, middleware)
}