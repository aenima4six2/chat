import * as actions from '../actions'

const defaultState = {
    messageText: ''
}

export default function (state = defaultState, action) {
    switch (action.type) {
        case actions.RESET_MESSAGE_INPUT:
            {
                return Object.assign({}, defaultState)
            }
        case actions.EDIT_MESSAGE_INPUT:
            {
                return Object.assign({}, state, {
                    messageText: action.messageText
                })
            }
        case actions.LEAVE_CHANNEL:
        case actions.LEAVE_CHAT:
            {
                return Object.assign({}, defaultState)
            }
        default:
            {
                return state
            }
    }
}