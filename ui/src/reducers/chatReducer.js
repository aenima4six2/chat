import * as actions from '../actions'

const defaultState = {
    channelName: ''
}

export default function (state = defaultState, action) {
    switch (action.type) {
        case actions.EDIT_CHANNEL:
            {
                return Object.assign({}, state, {
                    channelName: action.channelName
                })
            }
        case actions.LEAVE_CHANNEL:
        case actions.LEAVE_CHAT:
            {
                return Object.assign({}, defaultState)
            }
        default:
            {
                return state
            }
    }
}