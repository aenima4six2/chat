import * as actions from '../actions'

const defaultState = {
    users: []
}

export default function (state = defaultState, action) {
    switch (action.type) {
        case actions.CREATE_USER:
            {
                return Object.assign({}, state, {
                    users: [...state.users, action.user]
                })
            }
        case actions.REMOVE_USER:
            {
                return Object.assign({}, state, {
                    users: [...state.users.filter(x => x.id !== action.user.id), {
                        ...state.users.find(x => x.id === action.user.id),
                        available: false
                    }]
                })
            }
        case actions.LEAVE_CHANNEL:
            {
                const current = state.users.find(x => x.isCurrent)
                return Object.assign({}, state, {
                    users: [current]
                })
            }
        case actions.LEAVE_CHAT:
            {
                return Object.assign({}, defaultState)
            }
        default:
            {
                return state
            }
    }
}