import * as actions from '../actions'

const defaultState = {
    messages: []
}

export default function (state = defaultState, action) {
    switch (action.type) {
        case actions.ADD_MESSAGE:
            {
                return {
                    ...state,
                    messages: [...state.messages, action.message]
                }
            }
        case actions.LEAVE_CHANNEL:
        case actions.LEAVE_CHAT:
            {
                return { ...defaultState }
            }
        default:
            {
                return state
            }
    }
}