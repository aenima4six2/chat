import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import getStore from './reducers'

import App from './components/App'
import JoinChat from './components/JoinChat'
import Chat from './components/Chat'
import Login from './components/Login'

import { requireAuth, requireNoAuth } from './middleware'
import { Router, Route, browserHistory, IndexRedirect } from 'react-router'

import AuthService from './utils/AuthService'
export const auth = new AuthService('I9HmE4yOdbkSQy1T01GrBmkdiQ4q9Wae', 'jpdienst.auth0.com')

const authenticate = requireAuth(auth)
const noAuthenticate = requireNoAuth(auth)

ReactDOM.render(
  <Provider store={getStore(auth)}>
    <Router history={browserHistory}>
      <Route path="/" component={App} auth={auth}>
        <IndexRedirect to="/join" />
        <Route path="/join" component={JoinChat} onEnter={authenticate} />
        <Route path="/chat/:channel" component={Chat} onEnter={authenticate} />
        <Route path="/login" component={Login} onEnter={noAuthenticate} />
      </Route>
    </Router>
  </Provider>,
  document.getElementById('root')
)
