import * as actions from '../actions'
import * as messageActions from '../actions/messageActions'
import * as userActions from '../actions/userActions'
import io from 'socket.io-client'
import { apiBaseAddress } from '../utils/constants'
let socket

// Handle Socket.io communctation
export const sendMessages = store => next => action => {
    // Send message
    if (action.type === actions.JOIN_CHANNEL) {
        if (!socket) {
            //Connect
            socket = io(apiBaseAddress)

            //Join
            const users = store.getState().users.users
            const currentUser = users.find(x => x.isCurrent)
            socket.emit('join', { user: currentUser, roomName: action.channel })

            // Subscribe to user joins
            socket.on('active users', users => {
                users.forEach(user => next(userActions.createUser(user, false)))
            });

            // Subscribe history
            socket.on('message history', messages => {
                messages.forEach(message => next(messageActions.addMessage(message)))
            });

            // Subscribe to user joins
            socket.on('user joined', user => {
                next(userActions.createUser(user, false))
            });

            // Subscribe to user leaves
            socket.on('user left', user => {
                next(userActions.removeUser(user))
            });

            // Subscribe to new messages
            socket.on('recieve message', message => {
                next(messageActions.addMessage(message))
            });
        }
    } else if (action.type === actions.ADD_MESSAGE) {
        socket.emit('send message', action.message)
    } else if (action.type === actions.LEAVE_CHAT ||
        action.type === actions.LEAVE_CHANNEL) {
        if (socket) {
            socket.disconnect()
            socket = null
        }
    }

    // Next middleware
    next(action)
}

// validate authentication for private routes
export const requireAuth = auth => (nextState, replace) => {
    if (!auth.loggedIn()) {
        replace({
            pathname: '/login'
        })
    }
}

export const requireNoAuth = auth => (nextState, replace) => {
    if (auth.loggedIn()) {
        replace({
            pathname: '/join'
        })
    }
}