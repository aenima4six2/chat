export const createCurrentUser = (profile) => {
  return Object.assign({}, profile, {
    id: profile.user_id,
    isCurrent: true,
    available: true
  })
}