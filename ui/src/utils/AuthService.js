import Auth0Lock from 'auth0-lock'
import {
    browserHistory
} from 'react-router'
import {
    EventEmitter
} from 'events'

const LOGIN_REDIRECT_URL = 'http://localhost:3000/login'
const LOGGED_IN_URL = '/join'
const LOGGED_OUT_URL = '/login'

export default class AuthService extends EventEmitter {
    constructor(clientId, domain) {
        // Configure Auth0
        super()
        this.lock = new Auth0Lock(clientId, domain, {
            auth: {
                redirectUrl: LOGIN_REDIRECT_URL,
                responseType: 'token'
            }
        })

        // Add callback for lock `authenticated` event
        this.lock.on('authenticated', authResult => {

            // Load user profile
            this.lock.getProfile(authResult.idToken, (err, pro) => {
                if (err) {
                    console.log('Error loading the Profile', err)
                } else {
                    this.setProfile(pro)
                    this.setToken(authResult.idToken)
                    browserHistory.replace(LOGGED_IN_URL)
                }
            })
        })
    }

    login = () => {
        // Call the show method to display the widget.
        this.lock.show()
        this.emit('logged_in')
    }

    loggedIn = () => {
        // Checks if there is a saved token and it's still valid
        return !!this.getToken()
    }

    setProfile = (profile) => {
        if (profile) {
            localStorage.setItem('profile', JSON.stringify(profile))
            this.emit('profile_updated', profile)
        } else {
            localStorage.removeItem('profile');
            this.emit('profile_updated')
        }
    }

    getProfile() {
        const profile = localStorage.getItem('profile')
        return profile ? JSON.parse(profile) : null
    }

    setToken = (idToken) => {
        // Saves user token to local storage
        if (idToken) {
            localStorage.setItem('id_token', idToken)
        } else {
            localStorage.removeItem('id_token');
        }
    }

    getToken = () => {
        // Retrieves the user token from local storage
        return localStorage.getItem('id_token')
    }

    logout = () => {
        // Clear user token and profile data from local storage
        this.setProfile()
        this.setToken()
        this.emit('logged_out')

        // navigate to the home route
        browserHistory.replace(LOGGED_OUT_URL)
    }
}