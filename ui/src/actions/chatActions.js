import * as actions from './index'

export function editChannelName(channelName) {
    return {
        type: actions.EDIT_CHANNEL,
        channelName
    }
}

export function joinChannel(channel) {
    return {
        type: actions.JOIN_CHANNEL,
        channel
    }
}

export function leaveChannel(channel) {
    return {
        type: actions.LEAVE_CHANNEL,
        channel
    }
}

export function leaveChat() {
    return {
        type: actions.LEAVE_CHAT
    }
}