import * as actions from './index'

export function createUser(user, isCurrent) {
    return {
        type: actions.CREATE_USER,
        user: {...user, isCurrent, available: true}
    }
}

export function removeUser(user){
    return {
        type: actions.REMOVE_USER,
        user
    }
}