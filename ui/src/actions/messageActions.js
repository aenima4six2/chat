import * as actions from './index'

export function createMessage(text, user) {
    return addMessage({
        text: text,
        userId: user.id,
        username: user.name,
        date: new Date()
    })
}

export function addMessage(message) {
    return {
        type: actions.ADD_MESSAGE,
        message
    }
}