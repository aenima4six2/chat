import * as actions from './index'

export function inputUpdated(messageText){
    return {
        type: actions.EDIT_MESSAGE_INPUT,
        messageText
    }
}

export function resetInput(){
    return {
        type: actions.RESET_MESSAGE_INPUT
    }
}